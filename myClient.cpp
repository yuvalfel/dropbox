#include "myClient.h"

using namespace std;

void main()
{
	int SUCCESSFUL;
	WSAData WinSockData; //WinSock Object
	WORD DLLVERSION;

	DLLVERSION = MAKEWORD(2, 1);

	SUCCESSFUL = WSAStartup(DLLVERSION, &WinSockData);

	string RESPONSE;//Client Response To Server
	string CONVERTER;
	char MESSAGE[200];

	SOCKADDR_IN ADDRESS;//Address

	SOCKET sock;//Socket
	sock = socket(AF_INET, SOCK_STREAM, NULL);//Creating the Socket
	ADDRESS.sin_addr.s_addr = inet_addr("192.168.225.61");	//Link to Server IP
	ADDRESS.sin_family = AF_INET;
	ADDRESS.sin_port = htons(444);

	cout << "\n\tCLIENT: do you eant to connect this server? (Y/N)";
	cin >> RESPONSE;

	if (RESPONSE == "N" || RESPONSE == "n")
	{
		cout << "\n\tOK. QUITING INSTEAD";
	}
	else if (RESPONSE == "Y" || RESPONSE == "y")
	{
		connect(sock, (SOCKADDR*)&ADDRESS, sizeof(ADDRESS));
		SUCCESSFUL = recv(sock, MESSAGE, sizeof(MESSAGE), NULL);
		CONVERTER = MESSAGE;
		cout << "\n\tMessage from the server:\n\n\t" << CONVERTER << endl;

	}
	else
	{
		cout << "\n\tThat was an inappropriate RESPONSE";
	}
	cout << "\n\t";
	system("pause");
	exit(1);

}