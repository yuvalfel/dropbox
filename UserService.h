#pragma once
#include <iostream>
#include <string>
#include "User.h"
using namespace std;
class UserService
{
public:

	UserService();
	void addClient(User us);
	void removeClient(string usname);
	void login(string usname, string pass);
	void changeUserPass();
	void changeUserNumOfFiles();
	void changeUserSpaceLeft();
	void sort();
	bool ifUserExist(string name);
	void chagePassSecirety(string pass);
	~UserService();
};

