#pragma once
#include <iostream>
#include <string>
#include <fstream>
using namespace std;
class Encrypt
{
public:

	static ifstream encryptFile(ifstream L);
	static ifstream decryptFile(ifstream L);
	static string encryptFile(string pass);
	static string decryptFile(string pass);



};

